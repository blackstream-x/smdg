# Simple (pythonic) MarkDown Generator

_Modules to programmatically generate MarkDown source code in a pythonic way_


## Installation

smdg is available on PyPI: <https://pypi.org/project/smdg/>

```text
pip install smdg
```

Installation in a virtual environment is strongly recommended.


## Example usage

Use the classes in the **smdg.elements** module
to create MarkDown elements.

Some elements may be nested,
and the **smdg.elements.render()** function
can be used to return a MarkDown document
(consisting of the provided elements)
as a single string.


```text
>>> from smdg import elements as me
>>>
>>> h1 = me.Header(1, "Main header")
>>> h2 = me.Header(2, "Printing colors:")
>>> p = me.Paragraph("Lorem ipsum dolor sit amet,\nconsectetuer adipiscing elit.")
>>> pre = me.CodeBlock('def main() -> int:\n    """main function"""\n    return 0')
>>> ul = me.UnorderedList("Cyan", "Magenta", "Yellow", "Key")
>>>
>>> print(h1)
# Main header
>>> print(h2)
## Printing colors:
>>> print(p)
Lorem ipsum dolor sit amet,
consectetuer adipiscing elit.
>>>
>>> print(me.render(h1, p, pre, h2, ul))
# Main header

Lorem ipsum dolor sit amet,
consectetuer adipiscing elit.

    def main() -> int:
        """main function"""
        return 0

## Printing colors:

*   Cyan
*   Magenta
*   Yellow
*   Key
>>>
>>> print(me.BlockQuote(h1, p, pre, h2, ul))
> # Main header
>
> Lorem ipsum dolor sit amet,
> consectetuer adipiscing elit.
>
>     def main() -> int:
>         """main function"""
>         return 0
>
> ## Printing colors:
>
> *   Cyan
> *   Magenta
> *   Yellow
> *   Key
>>>
```


## Conformance

**smdg** aims for 100% conformance to the
[original MarkDown specification][original MarkDown spec].

Future plans include support of extensions like

*   [tables][GLFM tables] like GitHub, GitLab or every other major code platform
*   [fenced code blocks][python-markdown fenced code blocks]
*   [admonitions][python-markdown admonitions]
*   _(tbc)_


[original MarkDown spec]: https://daringfireball.net/projects/markdown/syntax
[GLFM tables]: https://docs.gitlab.com/ee/user/markdown.html#tables
[python-markdown fenced code blocks]: https://python-markdown.github.io/extensions/fenced_code_blocks/
[python-markdown admonitions]: https://python-markdown.github.io/extensions/admonition/
