#!/bin/bash

# Quick code check

echo -e "\n=== mypy ===\n"
python3 -m mypy . || exit
echo -e "\n=== pylint (tests) ===\n--- ignoring duplicate-code ---\n"
PYTHONPATH=src python3 -m pylint --disable=duplicate-code tests || echo "--- ignored issues in test modules ---"
echo -e "\n=== pylint (src)  ===\n"
PYTHONPATH=src python3 -m pylint --reports=y src || echo "--- found issues, but continuing ---"
echo -e "\n=== flake8 ===\n"
flake8 --exclude venv,.tox,.git || exit
echo -e "\n=== black ===\n"
black -l 79 --check . || black -l 79 --diff .
